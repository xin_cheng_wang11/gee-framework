# gee framework
## introduction
diy golang web framework
just for fun

## 参考链接
[前缀树算法实现路由匹配原理解析：Go 实现](https://jishuin.proginn.com/p/763bfbd2a2ba)

[七天用Go从零实现系列](https://geektutu.com/post/gee-day4.html)

[Go 数据结构和算法篇（十三）：字符串匹配之 Trie 树](https://geekr.dev/posts/go-trie-tree-algorithm)

## 注意
- /* 不能处理 / 请求
- /a/* 不能处理 /a和/a/请求，但可以处理/a/1 或者/a/1/2等等请求

## Release v0.0.2
- bugfix 

## Trie 树的应用
Trie 树适用于那些查找前缀匹配的字符串，比如敏感词过滤和搜索框联想功能。

### 敏感词过滤系统
首先运营在后台手动更新敏感词，底层通过 Tire 树构建敏感词库，然后当商家发布商品时，以商品标题+详情作为主串，
将敏感词库作为模式串，进行匹配，如果模式串和主串有匹配字符，则以此为起点，继续往后匹配，直到匹配出完整字符串，
然后标记为匹配出该敏感词（如果想嗅探所有敏感词，继续往后匹配），
否则将主串匹配起点位置往后移，从下一个字符开始，继续与模式串匹配。

### 搜索框联想功能
另外，搜索框的查询关键词联想功能也是基于 Trie 树实现的：

![google](https://coding-pic.oss-cn-hangzhou.aliyuncs.com/images/2022-05-07-img.png)

Google搜索框联想词, 进而可以扩展到浏览器网址输入自动补全、IDE 代码编辑器自动补全、输入法自动补全功能等。

## 静态文件配置
[Go Web 编程之 静态文件](https://www.cnblogs.com/darjun/p/12190173.html)
我这里采用的是`http.ServeContent`这个模块,而不是`http.FileServer`,我这里会有点问题。
而且我觉得直接返回文件内容会更好理解一些

> 实现一个简单的文件目录查看和下载
![](https://coding-pic.oss-cn-hangzhou.aliyuncs.com/images/2022-05-09-c3BYdm.png)
```go
package main

import (
	"flag"
	"log"
	"net/http"
)

var (
	ServeDir string
)

func init() {
	flag.StringVar(&ServeDir, "sd", "./", "the directory to serve")
}

func main() {
	flag.Parse()
	mux := http.NewServeMux()
	mux.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir(ServeDir))))

	server := &http.Server{
		Addr:    ":8080",
		Handler: mux,
	}

	if err := server.ListenAndServe(); err != nil {
		log.Fatal(err)
	}
}

```
在上面的代码中，我们构建了一个简单的文件服务器。编译之后，将想浏览的目录作为参数传给命令行选项，就可以浏览和下载该目录下的文件了：
```shell
./main.exe -sd D:/code/golang
```
可以将端口也作为命令行选项，这样做出一个通用的文件服务器，编译之后就可以在其它机器上使用了😀。

## 优化点
在ServeHTTP()中，每接收到一个请求都需要逐一匹配前缀来添加中间件，太影响性能了

获取某个请求要执行的中间件，不应该采用循环的方式，可以采用Trie树,或者在请求之前就通过一个map[string][]HandlerFunc来保存
,但在这里需要注意如请求/a/b和/a/b/,所以map的string要存两份

gin是在前缀树的节点中添加中间件的切片，这样在匹配动态路由并解析参数时，就可以同时获得各分组的中间件。




```go
var middlewares []HandlerFunc
for _, routerGroup := range engine.routerGroups {
    if strings.HasPrefix(req.URL.Path, routerGroup.prefix) {
        middlewares = append(middlewares, routerGroup.middlewares...)
    }
}
c.handlers = middlewares
```

## feat
- recover panic
- ![recover panic](https://coding-pic.oss-cn-hangzhou.aliyuncs.com/images/2022-05-07-L0Ov4C.png)

## 开发过程中遇到的bug
windows系统保留端口，导致端口被占用，通过netstat找不到端口占用
> bind: An attempt was made to access a socket in a way forbidden by its access permissions.
panic: listen tcp 127.0.0.1:9995: bind: An attempt was made to access a socket in a way forbidden by its access permissions

![](https://coding-pic.oss-cn-hangzhou.aliyuncs.com/images/2022-05-09-dA1EFP.png)

按照一般处理方法，首先在终端下通过指令查找1099端口的使用情况，结果却发现并未找使用。

netstat -ano | findstr "9995"
![](https://coding-pic.oss-cn-hangzhou.aliyuncs.com/images/2022-05-09-fbJCNX.png)
一番资料搜集，发现当我们开启Hyper-V后，系统默认会分配给一些保留端口供Hyper-V使用，碰巧1099就被保留了！！！
![](https://coding-pic.oss-cn-hangzhou.aliyuncs.com/images/2022-05-09-w6Av9F.png)
![](https://coding-pic.oss-cn-hangzhou.aliyuncs.com/images/2022-05-09-VuenIR.png)

那么我们只需要让系统不要保留9995端口即可（其他类似问题，也可以用相同方式处理）

[自 Vista 和 Windows Server 2008 以来 TCP/IP 的默认Windows端口范围已更改](https://docs.microsoft.com/zh-CN/troubleshoot/windows-server/networking/default-dynamic-port-range-tcpip-chang)

解决方案:改一下动态端口,注意要用管理员运行，然后重启电脑即可
![](https://coding-pic.oss-cn-hangzhou.aliyuncs.com/images/2022-05-09-OX9BQS.png)
```shell
netsh int ipv4 set dynamicport tcp start=50000 num=10000
netsh int ipv6 set dynamicport tcp start=50000 num=10000
```

## Git：git取消对某个文件的跟踪
一、通过修改.gitignore忽视某个文件但是发现这个文件还是会被踪，只对没有被git commit 过的文件有效，也就是说如果你的文件被commit过后你再修改gitignore，这个时候gitignore对这个文件是无效的，这个时候就需要取消对某个文件的跟踪。

二、取消对某个文件的跟踪步骤：

1、git rm -r -n --cached 文件或目录，列出你需要取消跟踪的文件，可以查看列表，检查下是否有误操作导致一些不应该被取消的文件取消了，是为了再次确认的。-r 表示递归，-n 表示先不删除，只是列出文件。


2、git rm -r --cached 文件，取消缓存不想要跟踪的文件

3、修改根目录.gitignore 文件，添加你希望忽略的文件/目录。

![](https://coding-pic.oss-cn-hangzhou.aliyuncs.com/images/2022-05-09-3rjeoS.png)

## 将自己的包上传到gitee并通过go get下载
参考链接：
https://mp.weixin.qq.com/s/VwLuqKXxaafhsw57aSY8TA

可以看到这是我的代码仓库,注意要设置成公开的
![](https://coding-pic.oss-cn-hangzhou.aliyuncs.com/images/2022-05-09-9iAD4w.png)
然后如果别人想要拉去我的库的话，执行命令
```go
go get -u -v gitee.com/xin_cheng_wang11/gee-framework
```
其中`gitee.com/xin_cheng_wang11/gee-framework`是我仓库的网址(注意没有https)
https://gitee.com/xin_cheng_wang11/gee-framework

有一点要注意的是，自己的库的go.mod中的module也要设置成gitee.com/xin_cheng_wang11/gee-framework
如果这两个不一致会有问题
![](https://coding-pic.oss-cn-hangzhou.aliyuncs.com/images/2022-05-09-oVBOxh.png)
我之前就不是一致的，然后拉的时候就出问题
![](https://coding-pic.oss-cn-hangzhou.aliyuncs.com/images/2022-05-09-Ml91yL.png)
然后我修改了go mod重新push到gitee，发现还是报这个错，我猜是go get的缓存
所以通过go env查看下缓存的路径
![](https://coding-pic.oss-cn-hangzhou.aliyuncs.com/images/2022-05-09-oTpSWM.png)
进到这个目录里把缓存删掉就可以了
![](https://coding-pic.oss-cn-hangzhou.aliyuncs.com/images/2022-05-09-oe3uZV.png)
再次执行，报另外的错了
![](https://coding-pic.oss-cn-hangzhou.aliyuncs.com/images/2022-05-09-NSdKsi.png)
这个是因为我修改了go mod，但是项目里引用的路径没有改，所以就会报这个错误，重新提交一下项目到gitee

在别的项目里拉，默认拉最新的分支
![](https://coding-pic.oss-cn-hangzhou.aliyuncs.com/images/2022-05-09-tIvRCs.png)
![](https://coding-pic.oss-cn-hangzhou.aliyuncs.com/images/2022-05-09-NpDWtR.png)
![](https://coding-pic.oss-cn-hangzhou.aliyuncs.com/images/2022-05-09-Y2RKaI.png)
使用
```go
r := gee.New()
r.GET("/", func(c *gee_context.Context) {
    c.String(http.StatusOK, "/")
})
r.GET("/a/c", func(c *gee_context.Context) {
    // expect /hello?name=geektutu
    c.String(http.StatusOK, "/a/d")
})

r.GET("/a/b", func(c *gee_context.Context) {
    // expect /hello?name=geektutu
    c.String(http.StatusOK, "/a/b")
})
r.GET("/a/a", func(c *gee_context.Context) {
    // expect /hello?name=geektutu
    c.String(http.StatusOK, "/a/a")
})
r.GET("/a/e", func(c *gee_context.Context) {
    // expect /hello?name=geektutu
    c.String(http.StatusOK, "/a/e")
})

r.GET("/a/b/c", func(c *gee_context.Context) {
    // expect /hello/geektutu
    c.String(http.StatusOK, "/a/b/c")
})
log.Panicln(r.Run("127.0.0.1:9995"))
```
![](https://coding-pic.oss-cn-hangzhou.aliyuncs.com/images/2022-05-09-xLuPfE.png)
