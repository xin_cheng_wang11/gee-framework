package gee

import (
	"gitee.com/xin_cheng_wang11/gee-framework/gee/gee_context"
	"gitee.com/xin_cheng_wang11/gee-framework/gee/middlewares"
	"io/fs"
	"net/http"
	"path"
	"strings"
)

// Engine implement the interface of ServeHTTP
type Engine struct {
	*RouterGroup
	router       *router
	routerGroups []*RouterGroup
}

type RouterGroup struct {
	prefix      string                    // like /a/b/c
	middlewares []gee_context.HandlerFunc // support middleware
	parent      *RouterGroup              // support nesting
	engine      *Engine                   // all groups share an Engine instance
}

// New is the constructor of gee.Engine
func New() *Engine {
	engine := &Engine{
		router: newRouter(),
	}
	engine.RouterGroup = &RouterGroup{engine: engine}
	engine.routerGroups = []*RouterGroup{engine.RouterGroup}
	return engine
}

// Group is defined to create a new RouterGroup
// remember all groups share the same Engine instance
func (routerGroup *RouterGroup) Group(prefix string) *RouterGroup {
	engine := routerGroup.engine
	newGroup := &RouterGroup{
		prefix: routerGroup.prefix + prefix,
		engine: engine,
		parent: routerGroup,
	}
	engine.routerGroups = append(engine.routerGroups, newGroup)
	return newGroup
}

// Use is defined to add middleware to the group
func (routerGroup *RouterGroup) Use(middlewares ...gee_context.HandlerFunc) {
	routerGroup.middlewares = append(routerGroup.middlewares, middlewares...)
}

//  /a/*   /a   /a/
//  /      /b   /b/
//*/
func (routerGroup *RouterGroup) addRoute(method string, pattern string, handler gee_context.HandlerFunc) {
	routerGroup.engine.router.addRoute(method, routerGroup.prefix+pattern, handler)
}

// GET defines the method to add GET request
func (routerGroup *RouterGroup) GET(pattern string, handler gee_context.HandlerFunc) {
	routerGroup.addRoute(http.MethodGet, pattern, handler)
}

// POST defines the method to add POST request
func (routerGroup *RouterGroup) POST(pattern string, handler gee_context.HandlerFunc) {
	routerGroup.addRoute(http.MethodPost, pattern, handler)
}

// PUT defines the method to add PUT request
func (routerGroup *RouterGroup) PUT(pattern string, handler gee_context.HandlerFunc) {
	routerGroup.addRoute(http.MethodPut, pattern, handler)
}

// DELETE defines the method to add DELETE request
func (routerGroup *RouterGroup) DELETE(pattern string, handler gee_context.HandlerFunc) {
	routerGroup.addRoute(http.MethodDelete, pattern, handler)
}

// create static handler
func (routerGroup *RouterGroup) createStaticHandler(fileSystem http.FileSystem) gee_context.HandlerFunc {
	return func(c *gee_context.Context) {
		var (
			file http.File
			fi   fs.FileInfo
			err  error
		)
		fileName := c.Param("filepath")
		// Check if file exists and/or if we have permission to access it
		if file, err = fileSystem.Open(fileName); err != nil {
			c.Status(http.StatusNotFound)
			return
		}
		// Stat 返回描述文件的 FileInfo 结构
		if fi, err = file.Stat(); err != nil {
			c.Status(http.StatusNotFound)
			return
		}
		// http.ServeContent除了接受参数http.ResponseWriter和http.Request，
		// 还需要文件名fileName，修改时间modTime和io.ReadSeeker接口类型的参数。
		// fileName是为了推断响应的content-type
		// modTime参数是为了设置响应的Last-Modified首部。如果请求中携带了If-Modified-Since首部，
		// ServeContent方法会根据modTime判断是否需要发送内容。
		// 如果需要发送内容，ServeContent方法从io.ReadSeeker接口重读取内容。*os.File实现了接口io.ReadSeeker。
		http.ServeContent(c.Writer, c.Req, fileName, fi.ModTime(), file)
	}
}

// Static serve static files
func (routerGroup *RouterGroup) Static(relativePath string, root string) {
	handler := routerGroup.createStaticHandler(http.Dir(root))
	urlPattern := path.Join(relativePath, "/*filepath")
	// Register GET handlers
	routerGroup.GET(urlPattern, handler)
}

// Run defines the method to start a http server
func (engine *Engine) Run(addr string) (err error) {
	engine.router.printRoutes()
	return http.ListenAndServe(addr, engine)
}

func (engine *Engine) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	c := gee_context.NewContext(w, req)
	var handlerFuncs []gee_context.HandlerFunc
	for _, routerGroup := range engine.routerGroups {
		if strings.HasPrefix(req.URL.Path, routerGroup.prefix) {
			handlerFuncs = append(handlerFuncs, routerGroup.middlewares...)
		}
	}
	c.Handlers = handlerFuncs
	engine.router.handle(c)
}

// Default use Logger() & Recovery middlewares
func Default() *Engine {
	engine := New()
	engine.Use(middlewares.Logger(), middlewares.Recovery())
	return engine
}
