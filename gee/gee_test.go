package gee

import (
	"fmt"
	"gitee.com/xin_cheng_wang11/gee-framework/gee/gee_context"
	"log"
	"net/http"
	"strings"
	"testing"
)

func TestRouter(t *testing.T) {
	r := newRouter()
	// r.addRoute("GET", "/", nil)
	r.addRoute("GET", "/", nil)
	r.addRoute("GET", "/hello/:name", nil)
	r.addRoute("GET", "/hi/:name", nil)
	r.addRoute("GET", "/assets/*filepath", nil)

	_, _ = r.getRoute("GET", "/assets/js/index.js")

}

func TestName(t *testing.T) {
	join := strings.Join([]string{"a", "b"}, "/")
	fmt.Println(join)
}

func TestGee(t *testing.T) {
	r := New()
	r.GET("/", func(ctx *gee_context.Context) {

	})
	r.GET("/a/c", func(c *gee_context.Context) {
		// expect /hello?name=geektutu
		c.String(http.StatusOK, "/a/d")
	})

	r.GET("/a/b", func(c *gee_context.Context) {
		// expect /hello?name=geektutu
		c.String(http.StatusOK, "/a/b")
	})
	r.GET("/a/a", func(c *gee_context.Context) {
		// expect /hello?name=geektutu
		c.String(http.StatusOK, "/a/a")
	})
	r.GET("/a/e", func(c *gee_context.Context) {
		// expect /hello?name=geektutu
		c.String(http.StatusOK, "/a/e")
	})

	r.GET("/a/b/c", func(c *gee_context.Context) {
		// expect /hello/geektutu
		c.String(http.StatusOK, "/a/b/c")
	})
	log.Panicln(r.Run("127.0.0.1:9995"))
}

func TestGee2(t *testing.T) {
	r := New()
	r.GET("/", func(ctx *gee_context.Context) {
		ctx.String(http.StatusOK, ctx.Path)
	})
	log.Panicln(r.Run("127.0.0.1:9995"))
}
