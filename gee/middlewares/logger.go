package middlewares

import (
	"gitee.com/xin_cheng_wang11/gee-framework/gee/gee_context"
	"log"
	"time"
)

func Logger() gee_context.HandlerFunc {
	return func(c *gee_context.Context) {
		// Start timer
		t := time.Now()
		c.Next()
		// Calculate resolution time
		log.Printf("[%d] %s in %v", c.StatusCode, c.Req.RequestURI, time.Since(t))
	}
}
