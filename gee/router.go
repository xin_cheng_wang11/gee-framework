package gee

import (
	"errors"
	"fmt"
	"gitee.com/xin_cheng_wang11/gee-framework/gee/gee_context"
	"log"
	"net/http"
	"sort"
	"strings"
)

type node struct {
	path     string           // 路由路径
	part     string           // 路由中由'/'分隔的部分
	children map[string]*node // 子节点
	isWild   bool             // 是否是通配符节点
}

var (
	methods = []string{http.MethodGet, http.MethodPost, http.MethodPut, http.MethodDelete}
)

func (n *node) String() string {
	return fmt.Sprintf("node{part=%s, part=%s, isWild=%t}", n.part, n.part, n.isWild)
}

func (n *node) travel(nodes *[]*node) {
	if n.path != "" {
		*nodes = append(*nodes, n)
	}
	keys := n.sort()
	for _, key := range keys {
		n.children[key].travel(nodes)
	}
}

func (n *node) sort() []string {
	// 得到各个key
	var keys []string
	for key := range n.children {
		keys = append(keys, key)
	}
	// 给key排序，从小到大
	sort.Sort(sort.StringSlice(keys))
	return keys
}

type router struct {
	roots    map[string]*node
	handlers map[string]gee_context.HandlerFunc
}

func newRouter() *router {
	r := &router{
		roots:    make(map[string]*node),
		handlers: make(map[string]gee_context.HandlerFunc),
	}
	for _, method := range methods {
		r.roots[method] = &node{children: make(map[string]*node)}
	}
	return r
}

// parsePath Only one * is allowed
func parsePath(pattern string) []string {
	vs := strings.Split(pattern, "/")

	parts := make([]string, 0)
	for _, item := range vs {
		if item != "" {
			parts = append(parts, item)
			if item[0] == '*' {
				break
			}
		}
	}
	return parts
}

// addRoute 绑定路由到handler
func (r *router) addRoute(method, path string, handler gee_context.HandlerFunc) {
	if path == "" {
		panic(errors.New("path should not be empty"))
	}
	parts := parsePath(path)
	root := r.roots[method]
	// path == "/"
	if len(parts) == 0 {
		key := r.getRouteKey(method, path)
		if root.path != "" && len(parts) == 0 {
			panic(errors.New(fmt.Sprintf("duplicate route declaration:%4s - %s", method, root.path)))
		}
		root.path = path
		r.handlers[key] = handler
		return
	}

	key := r.getRouteKey(method, path)
	// 将parts插入到路由树
	for _, part := range parts {
		if root.children[part] == nil {
			root.children[part] = &node{
				part:     part,
				children: make(map[string]*node, 0),
				isWild:   part[0] == ':' || part[0] == '*'}
		}

		root = root.children[part]
	}
	if root.path != "" {
		panic(errors.New(fmt.Sprintf("duplicate route declaration:%4s - %s", method, root.path)))
	}
	// 相当于前缀树的Stop,标识路由结束
	root.path = path
	// 绑定路由和handler
	r.handlers[key] = handler
}

// getRoute 获取路由树节点以及路由变量
// method用来判断属于哪一个方法路由树，path用来获取路由树节点和参数
func (r *router) getRoute(method, path string) (n *node, params map[string]string) {
	var (
		temp *node
	)
	params = map[string]string{}
	// path be like /a/b/c -> searchParts be like [a,b,c]
	searchParts := parsePath(path)

	n = r.roots[method]
	// path == "/"
	if len(searchParts) == 0 {
		if n.path != "" {
			return n, nil
		} else {
			return nil, nil
		}
	}
	// 在该方法的路由树上查找该路径
	temp = n
	for i, part := range searchParts {
		if temp.children[part] == nil {
			// 可能是动态路由或者没有这个路由
			// 查找child是否等于part
			for _, child := range temp.children {
				if child.isWild {
					// 添加参数
					if child.part[0] == '*' {
						params[child.part[1:]] = strings.Join(searchParts[i:], "/")
						return child, params
					}
					if child.part[0] == ':' {
						params[child.part[1:]] = part
						temp = child
						break
					}
				}
			}
			// 如果没有找到则就近匹配
			if temp != n && temp.path != "" {
				return temp, params
			}
		} else {
			temp = temp.children[part]
		}
	}
	if temp.path != "" {
		return temp, params
	}

	return nil, nil

}

// handle 用来绑定路由和handlerFunc
func (r *router) handle(c *gee_context.Context) {
	// 获取路由树节点和动态路由中的参数
	node, params := r.getRoute(c.Method, c.Path)
	if node != nil {
		c.Params = params
		key := r.getRouteKey(c.Method, node.path)
		c.Handlers = append(c.Handlers, r.handlers[key])
	} else {
		c.Handlers = []gee_context.HandlerFunc{func(ctx *gee_context.Context) {
			c.String(http.StatusNotFound, "404 NOT FOUND %s \n", c.Path)
		}}
	}
	c.Next()
}

func (r *router) getRouteKey(method string, path string) string {
	key := method + "-" + path
	return key
}

func (r *router) getRoutes(method string) []*node {
	root := r.roots[method]
	nodes := make([]*node, 0)
	root.travel(&nodes)
	return nodes
}

func (r *router) printRoutes() {
	for _, method := range methods {

		nodes := r.getRoutes(method)
		for _, node := range nodes {
			log.Printf("Route %4s - %s", method, node.path)
		}
	}
}
